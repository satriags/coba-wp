<?php
/**
 * LT Chain back compat functionality
 *
 * Prevents LT Chain from running on WordPress versions prior to 4.4,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.4.
 *
 * @package WordPress
 * @subpackage ltcore_setup
 * @since LT Chain 1.0
 */

/**
 * Prevent switching to LT Chain on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since LT Chain 1.0
 */
function ltcore_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'ltcore_upgrade_notice' );
}
add_action( 'after_switch_theme', 'ltcore_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * LT Ltcore on WordPress versions prior to 4.4.
 *
 * @since LT Ltcore 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ltcore_upgrade_notice() {
	printf(
		'<div class="error"><p>%s</p></div>',
		sprintf(
			/* translators: %s: The current WordPress version. */
			__( 'LT Ltcore requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'ltcore' ),
			$GLOBALS['wp_version']
		)
	);
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.4.
 *
 * @since LT Ltcore 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ltcore_customize() {
	wp_die(
		sprintf(
			/* translators: %s: The current WordPress version. */
			__( 'LT Ltcore requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'ltcore' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'ltcore_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.4.
 *
 * @since LT Ltcore 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ltcore_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die(
			sprintf(
				/* translators: %s: The current WordPress version. */
				__( 'LT Ltcore requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'ltcore' ),
				$GLOBALS['wp_version']
			)
		);
	}
}
add_action( 'template_redirect', 'ltcore_preview' );
