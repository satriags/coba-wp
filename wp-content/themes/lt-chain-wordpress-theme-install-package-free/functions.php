<?php
/**
 * LT Chain functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ltcore_setup
 */

if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'ltcore_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own ltcore_setup() function to override in a child theme.
	 *
	 * @since LT Chain 1.0
	 */
	function ltcore_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/ltcore
		 * If you're building a theme based on LT Chain, use a find and replace
		 * to change 'ltcore' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'ltcore' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since LT Chain 1.2
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#post-thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'ltcore' ),
				'social'  => __( 'Social Links Menu', 'ltcore' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
				'navigation-widgets',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://wordpress.org/support/article/post-formats/
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', ltcore_fonts_url() ) );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom color scheme.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Dark Gray', 'ltcore' ),
					'slug'  => 'dark-gray',
					'color' => '#1a1a1a',
				),
				array(
					'name'  => __( 'Medium Gray', 'ltcore' ),
					'slug'  => 'medium-gray',
					'color' => '#686868',
				),
				array(
					'name'  => __( 'Light Gray', 'ltcore' ),
					'slug'  => 'light-gray',
					'color' => '#e5e5e5',
				),
				array(
					'name'  => __( 'White', 'ltcore' ),
					'slug'  => 'white',
					'color' => '#fff',
				),
				array(
					'name'  => __( 'Blue Gray', 'ltcore' ),
					'slug'  => 'blue-gray',
					'color' => '#4d545c',
				),
				array(
					'name'  => __( 'Bright Blue', 'ltcore' ),
					'slug'  => 'bright-blue',
					'color' => '#007acc',
				),
				array(
					'name'  => __( 'Light Blue', 'ltcore' ),
					'slug'  => 'light-blue',
					'color' => '#9adffd',
				),
				array(
					'name'  => __( 'Dark Brown', 'ltcore' ),
					'slug'  => 'dark-brown',
					'color' => '#402b30',
				),
				array(
					'name'  => __( 'Medium Brown', 'ltcore' ),
					'slug'  => 'medium-brown',
					'color' => '#774e24',
				),
				array(
					'name'  => __( 'Dark Red', 'ltcore' ),
					'slug'  => 'dark-red',
					'color' => '#640c1f',
				),
				array(
					'name'  => __( 'Bright Red', 'ltcore' ),
					'slug'  => 'bright-red',
					'color' => '#ff675f',
				),
				array(
					'name'  => __( 'Yellow', 'ltcore' ),
					'slug'  => 'yellow',
					'color' => '#ffef8e',
				),
			)
		);

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for custom line height controls.
		add_theme_support( 'custom-line-height' );
	}
endif; // ltcore_setup()
add_action( 'after_setup_theme', 'ltcore_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since LT Chain 1.0
 */
function ltcore_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ltcore_content_width', 840 );
}
add_action( 'after_setup_theme', 'ltcore_content_width', 0 );

/**
 * Add preconnect for Google Fonts.
 *
 * @since LT Chain 1.6
 *
 * @param array  $urls          URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed.
 * @return array URLs to print for resource hints.
 */
function ltcore_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'ltcore-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'ltcore_resource_hints', 10, 2 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since LT Chain 1.0
 */
function ltcore_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'ltcore' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'ltcore' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'ltcore' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'ltcore' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'ltcore' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'ltcore' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'ltcore_widgets_init' );

if ( ! function_exists( 'ltcore_fonts_url' ) ) :
	/**
	 * Register Google fonts for LT Chain.
	 *
	 * Create your own ltcore_fonts_url() function to override in a child theme.
	 *
	 * @since LT Chain 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function ltcore_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Merriweather, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'ltcore' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Montserrat, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'ltcore' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'ltcore' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family'  => urlencode( implode( '|', $fonts ) ),
					'subset'  => urlencode( $subsets ),
					'display' => urlencode( 'fallback' ),
				),
				'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since LT Chain 1.0
 */
function ltcore_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'ltcore_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since LT Chain 1.0
 */
function ltcore_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'ltcore-fonts', ltcore_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '20201208' );

	// Theme stylesheet.
	wp_enqueue_style( 'ltcore-style', get_stylesheet_uri(), array(), '20201208' );

	// Theme block stylesheet.
	wp_enqueue_style( 'ltcore-block-style', get_template_directory_uri() . '/css/blocks.css', array( 'ltcore-style' ), '20190102' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'ltcore-ie', get_template_directory_uri() . '/css/ie.css', array( 'ltcore-style' ), '20170530' );
	wp_style_add_data( 'ltcore-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'ltcore-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'ltcore-style' ), '20170530' );
	wp_style_add_data( 'ltcore-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'ltcore-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'ltcore-style' ), '20170530' );
	wp_style_add_data( 'ltcore-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'ltcore-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'ltcore-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'ltcore-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20170530', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'ltcore-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20170530' );
	}

	wp_enqueue_script( 'ltcore-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20181217', true );

	wp_localize_script(
		'ltcore-script',
		'screenReaderText',
		array(
			'expand'   => __( 'expand child menu', 'ltcore' ),
			'collapse' => __( 'collapse child menu', 'ltcore' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'ltcore_scripts' );

/**
 * Enqueue styles for the block-based editor.
 *
 * @since LT Chain 1.6
 */
function ltcore_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'ltcore-block-editor-style', get_template_directory_uri() . '/css/editor-blocks.css', array(), '20201208' );
	// Add custom fonts.
	wp_enqueue_style( 'ltcore-fonts', ltcore_fonts_url(), array(), null );
}
add_action( 'enqueue_block_editor_assets', 'ltcore_block_editor_styles' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since LT Chain 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function ltcore_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'ltcore_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since LT Chain 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function ltcore_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Block Patterns.
 */
require get_template_directory() . '/inc/block-patterns.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since LT Chain 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function ltcore_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'ltcore_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since LT Chain 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function ltcore_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'ltcore_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since LT Chain 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function ltcore_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'ltcore_widget_tag_cloud_args' );


/* Top Head */

function wpb_widgets_init() {

	register_sidebar( array(

		 'name' => 'Head Top Menu',

		 'id' => 'custom-header-widget',

		 'before_widget' => '<div class="top-head-widget">',

		 'after_widget' => '</div>',

		 'before_title' => '<h2 class="title-widget">',

		 'after_title' => '</h2>',

	 ) );

	}

add_action( 'widgets_init', 'wpb_widgets_init' );


// Hook Bottom

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Bottom 1',
        'id'   => 'footer-1-widget',
        'description'   => 'Footer 1 widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Bottom 2',
        'id'   => 'footer-2-widget',
        'description'   => 'Footer 2 widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Bottom 3',
        'id'   => 'footer-3-widget',
        'description'   => 'Footer 3 widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Bottom 4',
        'id'   => 'footer-4-widget',
        'description'   => 'Footer 3 widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}
// Footer

register_sidebar( array(
    'name' => 'Custom Footer',
    'id' => 'custom-footer',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );




 // Customize Color Options
function theme_customize_register( $wp_customize ) {
    
	// Top Header Custom
	$wp_customize->add_setting('themes_top_header_bg_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_top_header_link_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',

	));

	$wp_customize->add_setting('themes_top_header_link_hover_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_setting('themes_top_header_text_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));


	// Header Custom
	$wp_customize->add_setting('themes_header_bar_bg_color', array(
		'default' => '#1d062f',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_header_bar_sticky_bg_color', array(
		'default' => '#1d062f',
		'transport' => 'refresh',
	));

	// Menu Custom
	$wp_customize->add_setting('themes_text_menu_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_link_menu_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_link_menu_hover_color', array(
		'default' => '#7373ff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_link_menu_item_color', array(
		'default' => '#000000',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_sub_menu_item_bg_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_canvas_menu_bg_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	// Footer Custom

	$wp_customize->add_setting('themes_footer_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_footer_link_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_footer_link_hover_color', array(
		'default' => '#7373ff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_footer_bg_color', array(
		'default' => '#1d062f',
		'transport' => 'refresh',
	));


	// copyright Custom
	$wp_customize->add_setting('themes_copyright_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_copyright_link_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_copyright_link_hover_color', array(
		'default' => '#7373ff',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_copyright_bg_color', array(
		'default' => '#1d062f',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('themes_bg_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	// Create Section
	$wp_customize->add_section('themes_custom_colors', array(
		'title' => __('Custom Color Variables', 'theme'),
		'priority' => 3,
	));

	// Create Section background
	$wp_customize->add_section('themes_background_colors', array(
		'title' => __('Background Colors', 'theme'),
		'priority' => 2,
	));

	// max-width
    $wp_customize->add_setting( 'max_width_setting', array(
        'default'        => '1170px',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ));
    $wp_customize->add_control( 'test_control', array(
        'label'      => __('Container Max Width (pixels or percent), ex: 1170px or 90%', 'themename'),
        'section'    =>  'title_tagline',
        'settings'   => 'max_width_setting',
    ));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_link_color_control', array(
		'label' => __('Top Head', 'theme'),
		'description' => __('Background Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_top_header_bg_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_btn_color_control', array(
		'description' => __('Text Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_top_header_text_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_btn_hover_color_control', array(
		'description' => __('Link color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_top_header_link_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_bg_color_control', array(
		'description' => __('Link hover color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_top_header_link_hover_color',
	) ) );


	// Header Bar Control
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_header_color_control', array(
		'label' => __('Header Bar', 'theme'),
		'description' => __( 'Background Color', 'theme' ),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_header_bar_bg_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_bg_color_sticky_control', array(
		'description' => __('Background color sticky', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_header_bar_sticky_bg_color',
	) ) );


	// Menu Control

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_menu_color_control', array(
		'label' => __('Menu Custom', 'theme'),
		'description' => __('Text color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_text_menu_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_header_bg_color_control', array(
		'description' => __( 'Link color', 'theme' ),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_link_menu_color',	
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_menu_hover_color_control', array(
		'description' => __('Selected/hover link color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_link_menu_hover_color',
	) ) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_menu_item_color_control', array(
		'label' => __('Sub Menu Custom', 'theme'),
		'description' => __('Text color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_link_menu_item_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_menu_item_color_sub_control', array(
		'description' => __('Background color sub', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_sub_menu_item_bg_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_canvas_menu_color_control', array(
		'description' => __('Background color Canvas', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_canvas_menu_bg_color',
	) ) );

	// Footer Control

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_footer_color_control', array(
		'label' => __('Footer Custom', 'theme'),
		'description' => __('Text Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_footer_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_footer_link_color_control', array(
		'description' => __('Link Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_footer_link_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_footer_link_hover_color_control', array(
		'description' => __('Link Hover Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_footer_link_hover_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_footer_bg_color_control', array(
		'description' => __('Background Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_footer_bg_color',
	) ) );


	// copyright Control

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_copyright_color_control', array(
		'label' => __('Copyright Custom', 'theme'),
		'description' => __('Text Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_copyright_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_copyright_link_color_control', array(
		'description' => __('Link Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_copyright_link_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_copyright_link_hover_color_control', array(
		'description' => __('Link Hover Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_copyright_link_hover_color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_copyright_bg_color_control', array(
		'description' => __('Background Color', 'theme'),
		'section' => 'themes_custom_colors',
		'settings' => 'themes_copyright_bg_color',
	) ) );
	
	// background body Control
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themes_bg_color_control', array(
		'description' => __('Background Color', 'theme'),
		'section' => 'themes_background_colors',
		'settings' => 'themes_bg_color',
	) ) );


}

add_action('customize_register', 'theme_customize_register');


// Output Customize CSS
function theme_customize_css() { ?>

	<style type="text/css">
		
		body {
			background-color: <?php echo get_theme_mod('themes_bg_color'); ?>;
		}
		
		body:not(elementor-page) .site-content,
		body .container {
			max-width: <?php echo get_theme_mod('max_width_setting'); ?>;
		}

		.top-head {
			background-color: <?php echo get_theme_mod('themes_top_header_bg_color'); ?>;
		}
		.top-head,
		.top-head ul li,
		.top-head ul li i,
		.top-head p,
		.top-head .textwidget {
			color: <?php echo get_theme_mod('themes_top_header_text_color'); ?>;
		}
		.top-head a {
			color: <?php echo get_theme_mod('themes_top_header_link_color'); ?>;
		}

		.top-head a:hover {
			color: <?php echo get_theme_mod('themes_top_header_link_hover_color'); ?>;
		}


		.wrap-head header.site-header {
		    background-color: <?php echo get_theme_mod('themes_header_bar_bg_color'); ?>;
		 }

		 .wrap-head.is-sticky header.site-header {
		    background-color: <?php echo get_theme_mod('themes_header_bar_sticky_bg_color'); ?>;
		 }

		.main-navigation .primary-menu {
			color: <?php echo get_theme_mod('themes_text_menu_color'); ?>;
		}

		.main-navigation .primary-menu > li > a {
			color: <?php echo get_theme_mod('themes_link_menu_color'); ?>;
		}
		
		.main-navigation .primary-menu li li a {
			color: <?php echo get_theme_mod('themes_link_menu_item_color'); ?>;
		}

		.main-navigation ul ul.sub-menu {
			background-color: <?php echo get_theme_mod('themes_sub_menu_item_bg_color'); ?>;
		}

		#site-header-menu.toggled-on {
			background-color: <?php echo get_theme_mod('themes_canvas_menu_bg_color'); ?>;
		}


		body .main-navigation li:hover > a, 
		body .main-navigation li.focus > a, 
		body .main-navigation li.current-menu-item > a, 
		body .main-navigation li.current-menu-parent > a {
			color: <?php echo get_theme_mod('themes_link_menu_hover_color'); ?>;
		}

		.site-footer .bottom,
		.site-footer .bottom h2,
		.site-footer .bottom p,
		.site-footer .bottom .textwidget,
		.site-footer .bottom ul li {
			color: <?php echo get_theme_mod('themes_footer_color'); ?>;
		}
		.site-footer .bottom a {
			color: <?php echo get_theme_mod('themes_footer_link_color'); ?>;
		}

		.site-footer .bottom a:hover {
			color: <?php echo get_theme_mod('themes_footer_link_hover_color'); ?>;
		}

		.site-footer .bottom {
			background-color: <?php echo get_theme_mod('themes_footer_bg_color'); ?>;
		}


		.site-footer .footer,
		.site-footer .footer p {
			color: <?php echo get_theme_mod('themes_copyright_color'); ?>;
		}
		.site-footer .footer a {
			color: <?php echo get_theme_mod('themes_copyright_link_color'); ?>;
		}

		.site-footer .footer a:hover {
			color: <?php echo get_theme_mod('themes_copyright_link_hover_color'); ?>;
		}

		.site-footer .footer {
			background-color: <?php echo get_theme_mod('themes_copyright_bg_color'); ?>;
		}

	</style>

<?php }


add_action('wp_head', 'theme_customize_css');


// Custom Site Title

add_theme_support('custom-logo');

function yourPrefix_custom_logo_setup()
{
    $defaults = array(
        'flex-height' => false,
        'flex-width' => false,
        'header-text' => array('site-title', 'yourPrefix-site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'yourPrefix_custom_logo_setup');


// Slogan

function showtitle_slogan() {
$showttlslogan = get_theme_mod('display_site_title');
    if ($showttlslogan == true) {
        ?>  
        <style type="text/css">
        .site-title { display:none;}
        </style>
    <?php
    }
}
add_action('wp_head', 'showtitle_slogan');



function my_customize_register() {     
  global $wp_customize;
  $wp_customize->remove_section( 'colors' );  //Modify this line as needed  
}

add_action( 'customize_register', 'my_customize_register', 11 );


// Section Access Pro

add_action( 'customize_register', 'gltheme_theme_customizer' );

function gltheme_theme_customizer( $wp_customize ) {

	class gltheme_Customize_Heading_Control extends WP_Customize_Control {

		public $type  = 'heading_1';

		public function render_content() {

			if ( ! empty( $this->label ) ) {
				if ( $this->type == 'heading_1' ) {

					echo '<h3 class="gltheme-heading-1-' . esc_attr( $this->color ) . '">' . esc_html( $this->label ) . '<h3>';

				} elseif ( $this->type == 'heading_2' ) { ?>

					<h3 class="gltheme-heading-2">
						<?php echo esc_html( $this->label ); ?>
					</h3>
				<?php
				}
			}

			if ( ! empty( $this->description ) ) {
				?>
				<p class="customize-control-description"><?php echo wp_kses_post( $this->description ); ?></p>
				<?php
			}

		} // render_content.

	} // Class gltheme_Customize_Heading_Control.

	class gltheme_Text_Control extends WP_Customize_Control {

		public $control_text = '';

		public function render_content() {

			if ( ! empty( $this->label ) ) {
				?>
				<span class="customize-control-title">
					<?php echo esc_html( $this->label ); ?>
				</span>
				<?php
			}

			if ( ! empty( $this->description ) ) {
				?>
				<span class="customize-control-description">
					<?php echo wp_kses_post( $this->description ); ?>
				</span>
				<?php
			}

			if ( ! empty( $this->control_text ) ) {
				?>
				<span class="gltheme-text-control-content">
					<?php echo wp_kses_post( $this->control_text ); ?>
				</span>
				<?php
			}

		}

	}

	
	/*
	* Firts Steps and links
	*/

	$wp_customize->add_section( 'gltheme_first_steps_links', array(
		'title'    => __( '[Getting Started | Help & Guides]', 'gltheme' ),
		'priority' => 1,
	));

	/* Links */
	$wp_customize->add_setting( 'gltheme_heading_first_step_links', array(
		'default'           => '',
		'sanitize_callback' => 'gltheme_sanitize_text',
	));
	
	$wp_customize->add_control( new gltheme_Customize_Heading_Control(
		$wp_customize,
		'gltheme_heading_first_step_links',
		array(
			'type'     => 'heading_1',
			'settings' => 'gltheme_heading_first_step_links',
			'section'  => 'gltheme_first_steps_links',
			'label'    => __( 'Getting Started | Helps & Guides', 'gltheme' ),
		)
	));

	// Promo Content & Links
		$wp_customize->add_setting( 'gltheme_rate_button', array( 'sanitize_callback' => 'gltheme_sanitize_text' ) );
		$wp_customize->add_control( new gltheme_Text_Control(
			$wp_customize,
			'gltheme_rate_button',
			array(
				'settings'     => 'gltheme_rate_button',
				'section'      => 'gltheme_first_steps_links',
				'control_text' => __( '<ul style="background: #40c6cc;color: #fff;padding: 20px;font-size: 14px;"><h3>Getting Started</h3><li>1. Create new page (such as <b>Home</b> and <b>Blog</b>) via Admin > Page.</li> <li>2. Go to Admin > Settings > Reading, set "Home Page" as your <b>Home</b> page, "Posts page" as your <b>Blog</b> page.</li><li>3. Set Menu via Admin > Appearance > Menu, create new Menu and set it at location "Primary Menu".</li> <li>4. Set your Logo & Site Title via AT Airus > Site Identify.<li>5. Custom Color/Font variables and anything else with LT Novel.</li> <li>* <a href="https://trk.elementor.com/6665" target="_blank" title="Download Elementor" style="color:#fff">Get Elementor</a> to build page layout quickly.</li></ul>
				<p><a style="font-size: 14px;color: #ffffff;font-weight: 600;background-color: #51d2cb;border: 3px solid #51d2cb;border-radius: 5px;padding: 5px;text-decoration: none" href="https://doc.ltheme.com/free-elementor-wordpress-theme-document/" target="_blank" title="Read full theme document">Documentation</a>
				<a style="font-size: 14px;color: #ffffff;font-weight: 600;background-color: #40c6cc;border: 3px solid #40c6cc;border-radius: 5px;padding: 5px;text-decoration: none" href="https://ltheme.com/wordpress-themes/" target="_blank" title="See other beauty Wordpress themes from us">See Other Themes</a></p>', 'gltheme' ),
			)
		));
	// Live demo.
    $wp_customize->add_setting( 'gltheme_link_buttons', array( 'sanitize_callback' => 'gltheme_sanitize_text' ) );
    $wp_customize->add_control( new gltheme_Text_Control(
        $wp_customize,
        'gltheme_link_buttons',
        array(
            'settings'     => 'gltheme_link_buttons',
            'section'      => 'gltheme_first_steps_links',
            'control_text' => '<a style="font-size: 14px !important;
	color: #ffffff;
	font-weight: 600;
	background-color: #ffc600;
	border: 3px solid #ffc600;
	border-radius: 5px;
	padding: 5px;text-decoration: none;" href="https://ltheme.com/project/lt-chain-free-responsive-nft-wordpress-theme" target="_blank" title="Get full website like demo with PRO version">' . __( 'Get PRO License', 'gltheme' ) . '</a>
	            <a style="font-size: 14px !important;
	color: #ffffff;
	font-weight: 600;
	background-color: #51d2cb;
	border: 3px solid #51d2cb;
	border-radius: 5px;
	padding: 5px;text-decoration: none;" href="https://ltheme.com/contact/" target="_blank" title="Any pre-sale questions? Contact Us">' . __( 'Contact Us', 'gltheme' ) . '</a>',
		)
    )); 
}

/*** Detech RTL ***/

function custom_rtl() {
	if ( is_rtl() ) {
		wp_enqueue_style( 'generatepress-rtl', trailingslashit( get_template_directory_uri() ) . 'css/style_rtl.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'custom_rtl', 100 );
