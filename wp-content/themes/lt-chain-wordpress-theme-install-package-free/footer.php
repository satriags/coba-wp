<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage ltcore_setup
 * @since LT Chain 1.0
 */
?>

		</div><!-- .site-content -->
		

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="bottom">
				<div class="container">
						<!-- 1/4 -->
						<div class="four columns footer1">
							<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-1-widget') ) ?>
						</div>
						<!-- /End 1/4 -->
						<!-- 2/4 -->
						<div class="four columns footer2">
							<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-2-widget') ) ?>
						</div>
						<!-- /End 2/4 -->
						<!-- 3/4 -->
						<div class="four columns footer3">
							<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-3-widget') ) ?>
						</div>
						<!-- /End 3/4 -->
						<!-- 4/4 -->
						<div class="four columns footer4">
							<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-4-widget') ) ?>
						</div>
						<!-- /End 4/4 -->
				</div>
			</div>
			<div class="footer">
				<div class="container">
				

				<?php if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'ltcore' ); ?>">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'social',
									'menu_class'     => 'social-links-menu',
									'depth'          => 1,
									'link_before'    => '<span class="screen-reader-text">',
									'link_after'     => '</span>',
								)
							);
						?>
					</nav><!-- .social-navigation -->
				<?php endif; ?>

				<div class="site-info">
					<?php
						/**
						 * Fires before the ltcore footer text for footer customization.
						 *
						 * @since LT Chain 1.0
						 */
						do_action( 'ltcore_credits' );
					?>
					<?php
					if ( function_exists( 'the_privacy_policy_link' ) ) {
						the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
					}
					?>
					<div class="lt-footer">
				        <p>
							<a href="https://ltheme.com/wordpress-themes/" target="_blank" title="Premium Wordpress theme" rel="author nofollow">Premium Wordpress theme</a> by <a href="https://ltheme.com/" target="_blank" title="Wordpress themes" rel="author nofollow">LTheme</a>
						</p>
			    	</div>
				</div><!-- .site-info -->
				</div><!-- footer container class -->
			</div><!-- footer class -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
